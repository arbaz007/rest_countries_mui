import { Box } from "@mui/material";
import Skeleton from "react-loading-skeleton";

let CardSkeleton = ({ loop }) => {
  return (
    <>
      {Array(loop)
        .fill(0)
        .map((_, i) => {
          return (
            <Box key={i} className="skeleton">
              <Skeleton height={"50%"} style={{ marginBottom: "0.5rem" }} duration={0.5} />
              <div className="content-skeleton">
                <Skeleton
                  duration={0.5}
                  count={4}
                  height={"1rem"}
                  width={"100%"}
                  style={{ marginBottom: "0.5rem" }}
                />
              </div>
            </Box>
          );
        })}
    </>
  );
};
export default CardSkeleton;
