import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ThemeContext from "./ThemeContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
let CountryCardDetails = () => {
  let [country, setCountry] = useState();
  let { id } = useParams();
  console.log(id);
  let navigate = useNavigate();
  useEffect(() => {
    async function getCountry() {
      let data = await fetch(`https://restcountries.com/v3.1/alpha/${id}`);
      data = await data.json();
      setCountry(data[0]);
    }
    getCountry();
  }, []);

  let x = {
    jpn: {
      official: "日本",
      common: "日本",
    },
  };
  let { dark } = useContext(ThemeContext);
  return (
    <>
      <button
        onClick={() => navigate(-1)}
        className={`back-btn ${dark ? "back-btn-dark" : "back-btn-light"}`}
      >
        {" "}
        <FontAwesomeIcon icon={faArrowLeft} style={{ marginLeft: "1rem" }} />
        Back
      </button>
      <div className="card-detail">
        <div className="card-detail-image">
          <img src={country?.flags?.svg} alt="" srcset="" />
        </div>
        <div
          className={`card-detail-content ${
            dark ? "card-detail-content-dark" : "card-detail-content-light"
          }`}
        >
          <h1>{country?.name?.common}</h1>
          <div className="detail-content">
            <div
              className={`detail-content-1 ${
                dark ? "detail-content-1-dark" : "detail-content-1-light"
              }`}
            >
              <p>
                <b>Native Name</b>:{" "}
                {country ? (
                  Object.values(country?.name?.nativeName)[0].common
                ) : (
                  <></>
                )}
              </p>
              <p>
                <b>Population</b>: {country?.population}
              </p>
              <p>
                <b>Region</b>: {country?.region}
              </p>
              <p>
                <b>Sub Region</b>:{country?.subregion}
              </p>
              <p>
                <b>Capital</b>: {country?.capital}
              </p>
            </div>
            <div
              className={`detail-content-2 ${
                dark ? "detail-content-2-dark" : "detail-content-2-light"
              }`}
            >
              <p>
                <b>Top Level Domain</b>: {country?.tld}
              </p>
              <p>
                <b>Currencies</b>:{" "}
                {country ? (
                  country?.currencies &&
                  Object.values(country?.currencies)[0].name
                ) : (
                  <></>
                )}
              </p>
              <p>
                <b>Languages</b>:{" "}
                {country ? Object.values(country?.languages).join(", ") : <></>}
              </p>
            </div>
          </div>
          <div
            className={`border-countries ${
              dark ? "border-countries-dark" : "border-countries-light"
            }`}
          >
            <b>Border Countries</b>:{" "}
            {country?.borders
              ? country?.borders.map((each) => <button>{each}</button>)
              : ""}
          </div>
        </div>
      </div>
    </>
  );
};

export default CountryCardDetails;
