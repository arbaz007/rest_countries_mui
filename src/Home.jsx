import CountryCard from "./CountryCard";
import noData from "./assets/noData.svg";
import Filter from "./Filter"
import CardSkeleton from "./CardSkeleton";
import { Box } from "@mui/material";


let Home = ({
  loader,   /* getting all neccessary props as an object... */
  country, 
  input,
  setInput,
  region,
  sort,
  setSort,
  handleSort,
  searchBy,
  SetRegion,
  subRegion,
  setSubRegion,
  handleSubRegion,
  selectedSubregion,
  setSelectedSubregion,
  handleSearch,
}) => {
  // calling handleSort function to sort data accordingly with conditional rendering....
  
  let countries = handleSort(sort, country).filter((each) => {
    if (region === "" || region === each.region) {
      if (
        (searchBy === "country" &&
          (input === "" ||
            each.name.common
              .toLowerCase()
              .includes(input.toLocaleLowerCase()))) ||
        (searchBy === "capital" &&
          (input === "" ||
            (each.capital &&
              each.capital[0]
                .toLocaleLowerCase()
                .includes(input.toLocaleLowerCase()))))
      ) {
        if (selectedSubregion === "" || selectedSubregion === each.subregion) {
          return each;
        }
      }
    }
  });
  return (
    //Filter Component that contains search input and region, subregion, population filter...
    <>
      <Filter
        input={input}         /* passing all neccessary props to Filter.... */
        setInput={setInput}
        region={region}
        SetRegion={SetRegion}
        subRegion={subRegion}
        setSubRegion={setSubRegion}
        selectedSubregion={selectedSubregion}
        setSelectedSubregion={setSelectedSubregion}
        handleSubRegion={handleSubRegion}
        sort={sort}
        setSort={setSort}
        handleSearch={handleSearch}
      />

      {/* Showing Skeleton component while content being loaded... */}

      {loader && (
        <Box sx={{display: "flex", flexWrap: "wrap", padding: "0 4rem", gap: "4rem"}}>
          <CardSkeleton loop={10} />
        </Box>
      )}
      {countries.length === 0 ? (
        <Box className="no-country">
          <img src={noData} alt="svg image..." />
        </Box>
      ) : (

        // return all country card component...

        <Box className="card-deck">
          {countries.map((each, index) => {
            return (
              <CountryCard        /* Passing all neccessary props to CountryCard componenet.... */
                key={index}
                flag={each?.flags?.png}
                name={each?.name?.common}
                pop={each?.population}
                reg={each?.region}
                cap={each?.capital}
                id={each?.cca2}
              />
            );
          })}
        </Box>
      )}
    </>
  );
};
export default Home;
