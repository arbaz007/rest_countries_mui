import { useNavigate } from "react-router-dom";
import { Card, CardMedia, CardContent, Typography } from "@mui/material";

let CountryCard = ({ flag, name, pop, reg, cap, id }) => {
  let navigate = useNavigate();
  let handleNavigate = () => {
    navigate(`/country/${id}`);
  };
  return (
    // returning all country card...

    <>
      <Card sx={{ width: 300, height: 350 }} onClick={handleNavigate}>
        <CardMedia sx={{ height: "50%" }} image={flag} />
        <CardContent sx={{ padding: "1.4rem" }}>
          <Typography
            variant="h5"
            component="h2"
            sx={{ marginBottom: "0.5rem" }}
          >
            {name}
          </Typography>
          <Typography
            variant="div"
            component={"div"}
            sx={{ marginBottom: "0.5rem" }}
          >
            <Typography variant="span">Population </Typography>:{" "}
            {pop.toLocaleString()}
          </Typography>
          <Typography
            variant="div"
            component={"div"}
            sx={{ marginBottom: "0.5rem" }}
          >
            <Typography variant="span">Region </Typography>: {reg}
          </Typography>
          <Typography
            variant="div"
            component={"div"}
            sx={{ marginBottom: "0.5rem" }}
          >
            <Typography variant="span">Capital </Typography>:
            {cap ? cap[0] : "no capital"}
          </Typography>
        </CardContent>
      </Card>
    </>
  );
};
export default CountryCard;
