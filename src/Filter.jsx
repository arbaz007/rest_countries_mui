import ThemeContext from "./ThemeContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { useContext } from "react";
import {
  TextField,
  Select,
  MenuItem,
  Divider,
  Box,
} from "@mui/material";

let Filter = ({
  input,      /* Getting all neccessary props from Home component.... */
  setInput,
  region,
  SetRegion,
  subRegion,
  selectedSubregion,
  setSort,
  setSelectedSubregion,
  handleSubRegion,
}) => {
  let { dark } = useContext(ThemeContext);
  let selectTheme = {
    width: "200px",
    background: dark ? "hsl(209, 23%, 22%)" : "#fff",
  };
  
  return (
    <>
      <Box className="inputs">
        {/* search filter component... */}
          <TextField
            fullWidth
            placeholder="Search for Country..."
            sx={{
              width: "560px",
              background: dark ? "hsl(209, 23%, 22%)" : "white",
              marginRight: "auto",
            }}
            value={input}
            onChange={(e) => setInput(e.target.value)}
            InputProps={{
              startAdornment: (
                <FontAwesomeIcon
                  icon={faMagnifyingGlass}
                  className="search-icon"
                />
              ),
            }}
          />

        {/*   <div className="searh-select">
            <select onChange={(e) => handleSearch(e.target.value)}>
              <option value="country">search by country</option>
              <option value={"capital"}>search by capital</option>
            </select>
          </div> */}

          {/* Region filter component.... */}
          <Select
            onChange={(e) => {
              SetRegion(e.target.value);
              e.target.value === ""
                ? setSelectedSubregion("")
                : handleSubRegion(e.target.value);
            }}
            sx={selectTheme}
            fullWidth
            displayEmpty
            value={region}
          >
            <MenuItem value="">Filter by Region</MenuItem>
            <Divider />
            <MenuItem value="Africa">Africa</MenuItem>
            <MenuItem value="Americas">Americas</MenuItem>
            <MenuItem value="Asia">Asia</MenuItem>
            <MenuItem value="Europe">Europe</MenuItem>
            <MenuItem value="Oceania">Oceania</MenuItem>
          </Select>

          {/* Sub region filter component... */}
          <Select
            name="subregion"
            id="subregion"
            onChange={(e) => setSelectedSubregion(e.target.value)}
            sx={selectTheme}
            fullWidth
            displayEmpty
            value={selectedSubregion}
          >
            <MenuItem value={""}>Choose Sub region</MenuItem>
            <Divider />
            {subRegion[region] &&
              subRegion[region].map((each, index) => {
                return (
                  <MenuItem key={index} value={each}>
                    {each}
                  </MenuItem>
                );
              })}
          </Select>

          {/* Sort Population component... */}

          <Select
            name="sort-population"
            onChange={(e) => setSort(e.target.value)}
            displayEmpty
            value={""}
            fullWidth
            sx={selectTheme}
          >
            <MenuItem value={""}>Sort by</MenuItem>
            <Divider />
            <MenuItem value={"ascending"}>population in Ascending</MenuItem>
            <MenuItem value={"descending"}>population in Descending</MenuItem>
            <MenuItem value={"area-asc"}>area in Ascending</MenuItem>
            <MenuItem value={"area-desc"}>area in Descending</MenuItem>
          </Select> 
      </Box>
    </>
  );
};
export default Filter;
