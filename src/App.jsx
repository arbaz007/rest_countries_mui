import { useContext, useEffect, useState } from "react";

import "./App.css";
import ThemeContext from "./ThemeContext";
import "react-loading-skeleton/dist/skeleton.css";
import Home from "./Home";
import { Route, Routes } from "react-router-dom";
import CountryCardDetails from "./CountryCardDetails";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  Box,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import Navbar from "./Navbar";
function App() {
  const [region, SetRegion] = useState("");
  const [input, setInput] = useState("");
  const [country, setCountry] = useState([]);
  const [error, setError] = useState(false);
  const [loader, setLoader] = useState(true);
  const [subRegion, setSubRegion] = useState({});
  const [sort, setSort] = useState("");
  const [selectedSubregion, setSelectedSubregion] = useState("");
  const [searchBy, setSearchBy] = useState("country");

  // fetching country api once...
  useEffect(() => {
    async function getCountry() {
      try {
        let data = await fetch("https://restcountries.com/v3.1/all");
        if (!data) {
          throw new Error("Failed to fetch data");
        }
        data = await data.json();
        setCountry(data);
        setLoader(false);
      } catch (error) {
        setError(true);
      }
    }
    getCountry();
  }, []);

  const { dark } = useContext(ThemeContext);

  let handleSubRegion = (region) => {
    let subReg = country.reduce((acc, curr) => {
      if (curr["region"] == region) {
        if (!acc[region]) {
          acc[region] = [];
          acc[region].push(curr["subregion"]);
        } else {
          if (!acc[region].includes(curr["subregion"])) {
            acc[region].push(curr["subregion"]);
          }
        }
      }
      return acc;
    }, {});
    setSubRegion(subReg);
  };

  let handleSearch = (value) => {
    setSearchBy(value);
  };


  //sorting all country data as per user choice...

  let handleSort = (value, array) => {
    let ans = array;
    if (value === "ascending") {
      ans = array.toSorted((a, b) => {
        return a.population - b.population;
      });
    }
    if (value === "descending") {
      ans = array.toSorted((a, b) => {
        return b.population - a.population;
      });
    }
    if (value === "area-asc") {
      ans = array.toSorted((a, b) => {
        return a.area - b.area;
      });
    }
    if (value === "area-desc") {
      ans = array.toSorted((a, b) => {
        return b.area - a.area;
      });
    }
    return ans;
  };

  // creating custom dark theme...
  // what color should be display in dark mode...
  
  const darkTheme = createTheme({
    palette: {
      mode: dark ? "dark" : "light",
      background: {
        paper: dark ? "hsl(207, 26%, 17%)" : "#fff",
        default: dark ? "red" : "#fff",
      },
      primary: {
        main: dark ? "#a2a2a2" : "#8a8a8a",
      },
    },
  });

  return (
    <Box className={dark ? "body-dark" : "body"}>
      <ThemeProvider theme={darkTheme}>
        <Navbar/>
        <Routes>
          <Route
            path={"/"}
            element={
              <Home
                loader={loader}     /* Providing neccessary props to Home Component.... */
                country={country}
                input={input}
                setInput={setInput}
                region={region}
                subregion={selectedSubregion}
                sort={sort}
                setSort={setSort}
                handleSort={handleSort}
                searchBy={searchBy}
                SetRegion={SetRegion}
                subRegion={subRegion}
                setSubRegion={setSubRegion}
                handleSubRegion={handleSubRegion}
                selectedSubregion={selectedSubregion}
                setSelectedSubregion={setSelectedSubregion}
                handleSearch={handleSearch}
              />
            }
          />
          <Route path="/country/:id" element={<CountryCardDetails />} />
        </Routes>
      </ThemeProvider>
    </Box>
  );
}

export default App;
