import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import { AppBar, Toolbar, Typography, Button } from "@mui/material";
import { useContext } from "react";
import ThemeContext from "./ThemeContext";

let Navbar = () => {
  const { dark, toggleTheme } = useContext(ThemeContext);
  return (
    <AppBar
      position="static"
      sx={{
        height: "100px",
        justifyContent: "center",
        background: dark ? "hsl(209, 23%, 22%)" : "#fff",
        marginBottom: "4rem",
        padding: "0 2rem",
      }}
    >
      <Toolbar>
        <Typography
          variant="h6"
          component="div"
          sx={{
            flexGrow: 1,
            fontSize: "1.8rem",
            color: dark ? "white" : "black",
          }}
        >
          Where in the world?
        </Typography>
        <Button
          color="inherit"
          onClick={toggleTheme}
          sx={{
            color: dark ? "white" : "black",
            "&:hover": { background: "none" },
          }}
          disableRipple
        >
          <FontAwesomeIcon icon={faMoon} style={{ marginRight: "1rem" }} />
          Dark Mode
        </Button>
      </Toolbar>
    </AppBar>
  );
};
export default Navbar;
